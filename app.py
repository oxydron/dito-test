from flask import Flask, escape, request, jsonify, render_template
from pymongo import MongoClient

client = MongoClient("mongodb://localhost:27017/")
db = client["autcomplete"]
events = db['events']
app = Flask("__name__")

strings = {
  'only_post':'Só é permitido método POST nesta rota.',
  'only_get':'Só é permitido método GET nesta rota.',
  'invalid_json': 'O JSON enviado é inválido, pois não possui o campo "%s".',
  'min_size': 'Tamanho mínimo da string de consulta para autocompletar é 2. Tamanho passado: %d.',
  'inserted': 'Evento inserido com sucesso!'
}

@app.route('/insert_event', methods=['POST'])
def insert_event():
  if request.method == 'POST':
    if 'event' not in request.json:
      return jsonify({'code':400,
        'message': strings['invalid_json'] % 'event'})

    if 'timestamp' not in request.json:
      return jsonify({'code':400,
        'message': strings['invalid_json'] % 'timestamp'})
      
    events.insert_one(request.json)
    return jsonify({'code':200, 'message':strings['inserted']})
  else:
    return jsonify({'code':400, 'message':strings['only_post']})


@app.route('/autocomplete', methods=['GET'])
def autocomplete():
  if request.method == 'GET':
    if 'autocomplete' not in request.json:
        return jsonify({'code':400,
          'message': strings['invalid_json'] % 'event'})

    autocomplete_str = request.json['autocomplete']
    autocomplete_size = len(autocomplete_str)

    if autocomplete_size < 2:
      return jsonify({'code':400,
          'message': strings['min_size'] % autocomplete_size})
    
    query = {"event":{"$regex": '^' + autocomplete_str}}
    
    completions = set()
    for event in events.find(query):
      completions.add(event['event'])
    
    completions = list(completions)
    completions.sort()
    return jsonify({'code':200, 'completions': completions})
  else:
    return jsonify({'code':400, 'message':strings['only_get']})
  