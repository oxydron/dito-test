# coding:utf8
import json
from urllib import request
from datetime import datetime

URL = 'https://storage.googleapis.com/dito-questions/events.json'

# Ignorando GMT -03:00 de Brasília por conveniência.
sorter = lambda x: datetime.strptime(x['timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')

def base_dict() -> dict:
    """Retorna o dicionário base para eventos da timeline."""
    return {
        "timestamp":      None,
        "revenue":        0.0,
        "transaction_id": None,
        "store_name":     None,
        "products":       []
    }


def product_dict() -> dict:
    """Retorna o dicionário base para produtos dos eventos da timeline"""
    return {'name': None, 'price': 0.0}


def flatten(custom_data: list) -> dict:
    """Simplifica lista de pares de chave e valor em um dicionário."""
    return {v['key']: v['value'] for v in custom_data}


def get_timeline(data: dict) -> dict:
    """Simplifica um dicionário de eventos."""
    aux = {}

    for e in data['events']:
        timestamp = e['timestamp']
        custom_data = flatten(e['custom_data'])
        transaction_id = custom_data['transaction_id']

        if transaction_id not in aux:
            aux[transaction_id] = base_dict()
            aux[transaction_id]['timestamp'] = timestamp
            aux[transaction_id]['transaction_id'] = transaction_id

        if e['event'] == 'comprou-produto':
            product = product_dict()
            product['name'] = custom_data['product_name']
            product['price'] = custom_data['product_price']
            aux[transaction_id]['products'].append(product)

        elif e['event'] == 'comprou':
            aux[transaction_id]['revenue'] = float(e['revenue'])
            aux[transaction_id]['store_name'] = custom_data['store_name']   

    output = list(aux.values())
    output.sort(key=sorter, reverse=True)
    return {'timeline':output}

def main():
    response = request.urlopen(URL)
    data = json.loads(response.read())    
    timeline = get_timeline(data)
    print(json.dumps(timeline, indent=2))

if __name__ == '__main__':
    main()